#include "User.h"
#include "../vk/Assert.h"
#include "../vk/chat/GUIException.h"

vk::safe::Settings User::settings;

void User::isValidUser(const User& user) noexcept(false){
	static int maxCharLenUsername	= atoi(settings["maxCharLenUsername"].c_str());
	static int maxCharLenPassword	= atoi(settings["maxCharLenPassword"].c_str());
	static int maxCharLenLogin		= atoi(settings["maxCharLenLogin"].c_str());
	static int minCharLenUsername	= atoi(settings["minCharLenUsername"].c_str());
	static int minCharLenPassword	= atoi(settings["minCharLenPassword"].c_str());
	static int minCharLenLogin		= atoi(settings["minCharLenLogin"].c_str());


	if (user.getLogin().length() >= maxCharLenLogin)
		throw std::runtime_error("Too long login. Max length is "+
								 std::to_string(maxCharLenLogin)+" symbols");
	if (user.getLogin().length() < minCharLenLogin)
		throw std::runtime_error("Too short login. It should be longer then " 
				+ std::to_string(minCharLenLogin) + " symbols");
	for (size_t i = 0; i < user.getLogin().length(); i++) {
		if (!User::isValidChar(user.getLogin().toStdString().c_str()[i])) {
			throw std::runtime_error("Forbidden symbols inside your login");
		}
	}


	if (user.getUsername().length() >= maxCharLenUsername)
		throw std::runtime_error("Too long login. Max length is " +
								 std::to_string(maxCharLenUsername) + " symbols");
	if (user.getUsername().length() < minCharLenUsername)
		throw std::runtime_error("Too short username. It should be longer then " 
				+ std::to_string(minCharLenUsername) + " symbols");
	for (size_t i = 0; i < user.getUsername().length(); i++) {
		if (!User::isValidChar(user.getUsername().toStdString().c_str()[i])) {
			throw std::runtime_error("Forbidden symbols inside your username");
		}
	}


	if (user.getPassword().length() >= maxCharLenPassword)
		throw std::runtime_error("Too long login. Max length is " +
								 std::to_string(maxCharLenPassword) + " symbols");
	if (user.getPassword().length() < minCharLenPassword)
		throw std::runtime_error("Too short password. It should be longer then " 
				+ std::to_string(minCharLenPassword) + " symbols");
	for (size_t i = 0; i < user.getPassword().length(); i++) {
		if (!User::isValidChar(user.getPassword().toStdString().c_str()[i])) {
			throw std::runtime_error("Forbidden symbols inside your password");
		}
	}


	if (user.getRePassword() != user.getPassword())
		throw std::runtime_error("Passwords do not match");
}

bool User::isValidChar(int s) noexcept{
	return (s >= 'A' && s <= 'Z') || (s >= 'a' && s <= 'z') || (s >= '0' && s <= '9') || s == '_' || s == '-';
}

const vk::safe::Settings& User::getSettings() noexcept(false){
	if (settings.isEmpty())
		throw vk::exception("The settings not initialized",__FILE__,__FUNCTION__,__LINE__);
	return settings;
}

User::User(string_v username, string_v login, string_v password, string_v repassword) noexcept(false){
	if (static bool is_settings_init = false; !is_settings_init) {
		settings.setPath("users_settings.probj").loadSettings();
		is_settings_init = true;
	}
	setUsername(username);
	setLogin(login);
	setPassword(password);
	setRePassword(repassword);
}

void User::setUsername(string_v username) noexcept(false){
	static int max_len = atoi(settings["maxCharLenUsername"].c_str());
	if (strlen(username) > max_len)
		throw vk::exception("Too big length of the username", __FILE__, __FUNCTION__, __LINE__);
	this->username = username;
}

const User::string& User::getUsername() const noexcept{
	return this->username;
}

void User::setLogin(string_v login) noexcept(false){
	static int max_len = atoi(settings["maxCharLenLogin"].c_str());
	if (strlen(login) > max_len)
		throw vk::exception("Too big length of the login", __FILE__, __FUNCTION__, __LINE__);
	this->login = login;
}

const User::string& User::getLogin() const noexcept{
	return this->login;
}

void User::setPassword(string_v password) noexcept(false){
	static int max_len = atoi(settings["maxCharLenPassword"].c_str());
	if (strlen(password) > max_len)
		throw vk::exception("Too big length of the password", __FILE__, __FUNCTION__, __LINE__);
	this->password = password;
}
const User::string& User::getPassword() const noexcept{
	return this->password;
}

void User::setRePassword(string_v repassword) noexcept(false){
	this->repassword = repassword;
}

const User::string& User::getRePassword() const noexcept{
	return repassword;
}

User::User(const User& obj) noexcept{
	*this = obj;
}

User::User(User&& obj) noexcept{
	*this = std::move(obj);
}

User& User::operator=(const User& obj) noexcept{
	this->username	= obj.username;
	this->login		= obj.login;
	this->password	= obj.password;
	this->repassword = obj.repassword;
	return *this;
}

User& User::operator=(User&& obj) noexcept{
	this->username	= std::move(obj.username);
	this->login		= std::move(obj.login);
	this->password	= std::move(obj.password);
	this->repassword = std::move(obj.repassword);
	return *this;
}
