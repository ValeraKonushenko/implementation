#pragma once
#include <string>
#include "../vk/codeArchitecture/Settings.h"
#include <qstring.h>
class User final{
public:
	typedef QString string;
	typedef const char* string_v;
private:
	//like iside the table of the DB
	static vk::safe::Settings settings;
	string username;
	string login;
	string password;
	string repassword;
public:
	static void		isValidUser	(const User& user)	noexcept(false);
	static bool		isValidChar	(int smb)			noexcept;
	static const vk::safe::Settings& getSettings()	noexcept(false);
	User(string_v username = "", string_v login = "",	
		 string_v password = "", string_v repassword = "")noexcept(false);
	void			setUsername	(string_v username)	noexcept(false);
	const string&	getUsername	()					const noexcept;
	void			setLogin	(string_v login)	noexcept(false);
	const string&	getLogin	()					const noexcept;
	void			setPassword	(string_v password)	noexcept(false);
	const string&	getPassword	()					const noexcept;
	void			setRePassword(string_v password)	noexcept(false);
	const string&	getRePassword()					const noexcept;
	~User()											= default;
	User(const User& obj)							noexcept;
	User(User&& obj)								noexcept;
	User& operator=(const User& obj)				noexcept;
	User& operator=(User&& obj)						noexcept;
};