#include "../vk/chat/GUIException.h"
#include "SignUp.h"
#include <qmessagebox.h>

SignUp::SignUp(QWidget *parent)
	: QDialog(parent){
	ui.setupUi(this);
	setWindowFlag(Qt::WindowType::MSWindowsFixedSizeDialogHint);
	setUsername("Valera");
	setLogin("mega_kach228");
	setPassword("123123");
	setRePassword("123123");
}

QString		SignUp::getUsername		()				const noexcept{
	return ui.lineEdit_username->text();
}
void		SignUp::setUsername		(QString v)		noexcept{
	ui.lineEdit_username->setText(v);
}
QString		SignUp::getLogin		()				const noexcept{
	return ui.lineEdit_login->text();
}
void		SignUp::setLogin		(QString v)		noexcept {
	ui.lineEdit_login->setText(v);
}
QString		SignUp::getPassword		()				const noexcept{
	return ui.lineEdit_password->text();
}
void		SignUp::setPassword		(QString v)		noexcept {
	ui.lineEdit_password->setText(v);
}
QString		SignUp::getRePassword	()				const noexcept{
	return ui.lineEdit_repassword->text();
}
void		SignUp::setRePassword	(QString v)		noexcept {
	ui.lineEdit_repassword->setText(v);
}

void SignUp::on_btnSignUp_clicked() {

	try {
		model.setUser(User(
			getUsername().toLocal8Bit().data(),
			getLogin().toLocal8Bit().data(),
			getPassword().toLocal8Bit().data(),
			getRePassword().toLocal8Bit().data()
		));
		model.registry();
		if (model.getMessage() != "")
			this->sendMessage(model.getMessage(),0);
		else
			close();
	} catch (const vk::GUIException&ex) {
		sendMessage(ex.what(), 0);
	} catch (const std::runtime_error& ex) {
		QMessageBox::critical(this->parentWidget(), "Critical error", ex.what());
	}
}

SignUp::~SignUp(){
	
}

void SignUp::sendMessage(QString message, bool is_good) noexcept{
	ui.label_message->setText(message);
}
