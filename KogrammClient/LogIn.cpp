#include "LogIn.h"
#include "SignUp.h"
#include "../vk/chat/VKCPv2.h"
#include "../vk/parser/ObjectParser.h"
#include "../vk/chat/GUIException.h"
#include <qmessagebox.h>
#include <fstream>
vk::safe::Settings LogIn::settings{};

void LogIn::setAutoLoginFile(const User& user) noexcept(false){
	vk::unsafe::ObjectParser parser;
	parser.push("login",user.getLogin().toLocal8Bit().data());
	parser.push("password",user.getPassword().toLocal8Bit().data());
	parser.push("username",user.getUsername().toLocal8Bit().data());
	std::fstream file(settings["auto_login_path"], std::ios::out);
	auto s = parser.inspect("\r\n");
	file << s.data();
	file.close();
}
User LogIn::getAutoLoginFile() noexcept(false){
	vk::unsafe::ObjectParser parser;
	std::fstream file(settings["auto_login_path"], std::ios::in);
	if (!file.is_open())throw std::runtime_error("no such file");
	file.seekg(0, std::ios::end);
	size_t fsize = file.tellg();
	file.seekg(0, std::ios::beg);
	char* buff = new char[fsize + 1]{};
	file.read(buff,fsize);
	parser.setData(buff);
	file.close();
	delete[]buff;
	User user(parser["username"]->data(),
			  parser["login"]->data(), parser["password"]->data());
	return user;
}
User LogIn::getUserData() const noexcept{
	return user_data;
}
bool LogIn::isCloseButtonPressed() const noexcept{
	return is_close_button_pressed;
}
bool LogIn::isLogin() noexcept{
	return is_login;
}
void LogIn::closeEvent(QCloseEvent* event){
	is_close_button_pressed = true;
}
void LogIn::printMessage(QString str, bool is_good) const noexcept{
	ui.label_message->setText(str);
}


void LogIn::goLogin(const User* user) noexcept(false){
	static const QString	host = settings["host"].toCCharp();
	static const long long	port = settings["port"].toLongLong();
	static const int		wait = settings["wait_for_connect"].toInt();
	static const QString	platform = settings["platform"].toCCharp();

	QString login	= getLogin();
	QString pasword = getPassword();
	if (user) {
		login	= user->getLogin();
		pasword = user->getPassword();
	}

	io.open(host, port, wait);
	io.write(
		IOTcpClient::MsgType::log_in_data,
		platform.toLocal8Bit().data(),
		QString("login=" + login + ";password=" + pasword + ";")
	);
	io.waitForReadyRead(-1);


	vk::unsafe::Container_c body;
	vk::proto::VKCPv2::Header header;
	io.read(header, body);
	vk::unsafe::ObjectParser parser(body.toString().c_str());
	if (strcmp(parser["status"]->data(), "ok") == 0) {
		this->printMessage("You are logged in!", true);
		is_login = true;
		user_data.setLogin(login.toLocal8Bit().data());
		user_data.setPassword(pasword.toLocal8Bit().data());
		user_data.setUsername(parser["username"]->data());
		if (isCheckedAutoLogin())
			LogIn::setAutoLoginFile(user_data);
		this->close();
	} else {
		this->printMessage(QString("Error: ") + parser["status"]->data(), false);
	}
	io.close();
}
void LogIn::goAutoLogin() noexcept(false){
	try {
		User user = LogIn::getAutoLoginFile();
		setLogin(user.getLogin());
		setPassword(user.getPassword());
	} catch (const std::exception&) {
		//nothing to do. Continue manual log in
	}
}

void LogIn::showEvent(QShowEvent* event){
	goAutoLogin();
	ui.checkBox_auto_login->setChecked(1);
}

QString LogIn::getLogin() const noexcept{
	return ui.lineEdit_login->text();
}
QString LogIn::getPassword() const noexcept{
	return ui.lineEdit_password->text();
}

void LogIn::setLogin(QString v) const noexcept{
	ui.lineEdit_login->setText(v);
}
void LogIn::setPassword(QString v) const noexcept{
	ui.lineEdit_password->setText(v);
}

bool LogIn::isCheckedAutoLogin() const noexcept{
	return ui.checkBox_auto_login->checkState() == Qt::CheckState::Checked;
}


void LogIn::on_pushButton_LogIn_clicked(){
	try {
		goLogin();
	} catch (const std::runtime_error& ex) {
		QMessageBox::critical(this->parentWidget(), "Critical error", ex.what());
	}
}
void LogIn::on_pushButton_sign_up_ref_clicked() {
	this->hide();
	SignUp w(this);
	w.exec();
	this->show();
}


LogIn::LogIn(QWidget* parent)noexcept(false)
	: QWidget(parent) {
	ui.setupUi(this);
	setWindowFlag(Qt::WindowType::MSWindowsFixedSizeDialogHint);
	
	settings.setPath("config.probj").loadSettings();
	is_close_button_pressed = false;
	is_login = false;
}
LogIn::~LogIn(){
}
