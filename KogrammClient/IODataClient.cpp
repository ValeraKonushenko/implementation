#include "IODataClient.h"
#include "../vk/Assert.h"
#include "../vk/chat/GUIException.h"

void IOTcpClient::__init__() noexcept{
	socket = nullptr;
}

void IOTcpClient::disconnected() {
}
void IOTcpClient::connected(){
}
void IOTcpClient::readyRead(){
}




void IOTcpClient::write(MsgType type, const char* platform, QString body) noexcept(false){
	this->write(type, platform, body.toLocal8Bit().data(), body.length());
}

void IOTcpClient::write(MsgType type, const char* platform, const char* data, size_t len) noexcept(false){
	using namespace vk::proto;
	using namespace vk::unsafe;

	auto proto = VKCPv2::generate(VKCPv2::proto_version,type,len,platform,{0});
	socket->write(proto.data(), VKCPv2::size_of_header);
	socket->write(data,len);
}

void IOTcpClient::read(Header& header, Container& body) noexcept(false){
	Container header_raw(vk::proto::VKCPv2::size_of_header);
	socket->read(header_raw.data(), header_raw.getSize());
	header = vk::proto::VKCPv2::parse(header_raw);
	body.reserve(header.body_size);
	socket->read(body, header.body_size);
}

bool IOTcpClient::waitForBytesWritten(int msec) const noexcept{
	return socket->waitForBytesWritten(msec);
}
bool IOTcpClient::waitForConnected(int msec) const noexcept{
	return socket->waitForConnected(msec);
}
bool IOTcpClient::waitForDisconnected(int msec) const noexcept{
	return socket->waitForDisconnected(msec);
}
bool IOTcpClient::waitForReadyRead(int msec) const noexcept{
	return socket->waitForReadyRead(msec);
}
bool IOTcpClient::isOpen() const noexcept{
	return socket && socket->isOpen();
}

void IOTcpClient::open(QString host, long long port, int wait) noexcept(false){
	if (isOpen())close();
	socket = new QTcpSocket();
	connect(socket, &QIODevice::readyRead, this, &IOTcpClient::readyRead);
	socket->connectToHost(host, port);
	if (!socket->waitForConnected(wait)) {
		throw vk::GUIException(std::string(("The client can not to connecto to the server. Error: " + socket->errorString()).toLocal8Bit().data()));
	}
}

void IOTcpClient::close() noexcept{
	if (socket && socket->isOpen())
		socket->abort();
	delete socket;
	__init__();
}

QString IOTcpClient::peerHost() const noexcept{
	return socket->peerAddress().toString();
}

quint16 IOTcpClient::peerPort() const noexcept{
	return socket->peerPort();
}

IOTcpClient::IOTcpClient() noexcept{
	__init__();
}

IOTcpClient::~IOTcpClient(){
	close();
}