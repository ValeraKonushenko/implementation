#include "Program.h"
#include "../vk/chat/GUIException.h"
#include <Windows.h>
int main(int argc, char **argv){
    try {
        Program p(argc, argv);
        return p.exec();        
    } catch (const std::runtime_error& ex) {
        MessageBoxA(nullptr, ex.what(), "Critical error!", MB_ICONERROR);
    } catch (const vk::GUIException& ex) {
        MessageBoxA(nullptr, ex.what(), "GUI error", MB_ICONERROR);
    } catch (...) {
        MessageBoxA(nullptr, "Has occured some error", "Critical error!", MB_ICONERROR);
    }
    return -1;
}
