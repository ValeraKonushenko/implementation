#pragma once
#include "KogrammClient.h"
#include "LogIn.h"
#include "../vk/Assert.h"
#include <QtWidgets/QApplication>
#include <qmessagebox.h>
class Program final {
private:
	int exec_res;

	QApplication app;
	LogIn log_in;
	
public:
	int exec()noexcept(false);
	Program(int argc, char** argv)noexcept(false);
	~Program();
	Program(const Program&)				= delete;
	Program(Program&&)					= delete;
	Program& operator=(const Program&)	= delete;
	Program& operator=(Program&&)		= delete;
};