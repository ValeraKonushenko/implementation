#pragma once

#include "ui_LogIn.h"
#include "../vk/codeArchitecture/Settings.h"
#include "../general/User.h"
#include "IODataClient.h"
#include <QWidget>
#include <qtcpsocket.h>

class LogIn : public QWidget{
private:
	Q_OBJECT
	static vk::safe::Settings settings;

	Ui::LogIn		ui;
	IOTcpClient		io;
	bool			is_close_button_pressed;
	bool			is_login;
	User			user_data;

	void			goLogin		(const User* user = nullptr)noexcept(false);
	void			goAutoLogin					()				noexcept(false);

	virtual void	showEvent(QShowEvent* event);
public://working with GUI
	QString			getLogin					()				const noexcept;
	QString			getPassword					()				const noexcept;
	void			setLogin					(QString v)				const noexcept;
	void			setPassword					(QString v)				const noexcept;
	bool			isCheckedAutoLogin			()				const noexcept;
	void			printMessage				(QString str, bool is_good)const noexcept;
private slots:
	void			on_pushButton_sign_up_ref_clicked();
	void			on_pushButton_LogIn_clicked	();
public:
	static void		setAutoLoginFile			(const User& user)	noexcept(false);
	static User		getAutoLoginFile			()					noexcept(false);
	User			getUserData					()				const noexcept;
	bool			isCloseButtonPressed		()				const noexcept;
	bool			isLogin						()				noexcept;
	virtual void	closeEvent					(QCloseEvent* event);
	LogIn(QWidget *parent = Q_NULLPTR)noexcept(false);
	~LogIn();
};