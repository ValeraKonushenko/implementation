#include "Program.h"
#include "LogIn.h"

int Program::exec() noexcept(false){
	LogIn log_in;
	do {
		log_in.show();
		exec_res = app.exec();
		if (log_in.isCloseButtonPressed() && !log_in.isLogin())
			return exec_res;
	} while (!log_in.isLogin());

	KogrammClient main_wnd;
	main_wnd.show();
	exec_res = app.exec();

	return exec_res;
}

Program::Program(int argc, char** argv) noexcept(false) :
	app(argc, argv){
	exec_res = 0;
}
Program::~Program(){
}
