#pragma once
#include "../general/User.h"
#include "IODataClient.h"
#include "../vk/codeArchitecture/Settings.h"
class SignUp_Model {
private:
	User user;
	IOTcpClient io;
	QString message;
public:
	vk::safe::Settings settings;
	
	void setUser(const User&user)noexcept;
	const User& getUser()const noexcept;
	void registry()noexcept(false);
	QString getMessage()const noexcept;

	SignUp_Model()noexcept(false);
	~SignUp_Model();
	SignUp_Model(const SignUp_Model& obj) = delete;
	SignUp_Model(SignUp_Model&& obj) = delete;
	SignUp_Model& operator=(const SignUp_Model& obj) = delete;
	SignUp_Model& operator=(SignUp_Model&& obj) = delete;
};