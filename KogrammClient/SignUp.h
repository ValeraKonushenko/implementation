#pragma once

#include <qdialog.h>
#include "ui_SignUp.h"
#include "SignUp_Model.h"

class SignUp : public QDialog{
private:
	Ui::SignUp ui;
	Q_OBJECT
	SignUp_Model model;
public:
	SignUp(QWidget *parent = Q_NULLPTR);
	~SignUp();


private://GUI
	QString		getUsername		()								const noexcept;
	void		setUsername		(QString)						noexcept;
	QString		getLogin		()								const noexcept;
	void		setLogin		(QString)						noexcept;
	QString		getPassword		()								const noexcept;
	void		setPassword		(QString)						noexcept;
	QString		getRePassword	()								const noexcept;
	void		setRePassword	(QString)						noexcept;
	void		sendMessage		(QString message, bool is_good)	noexcept;

public slots:
	void on_btnSignUp_clicked();
};
