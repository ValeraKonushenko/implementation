#include "SignUp_Model.h"
#include "../vk/chat/GUIException.h"
#include "LogIn.h"

void SignUp_Model::setUser(const User& user) noexcept{
	this->user = user;
}

const User& SignUp_Model::getUser() const noexcept{
	return user;
}

void SignUp_Model::registry() noexcept(false){
	message = "";
	try {
		User::isValidUser(user);
	} catch (const std::runtime_error&ex) {
		throw vk::GUIException(ex.what());
	}
	
	

	static const QString	host = settings["host"].toCCharp();
	static const long long	port = settings["port"].toLongLong();
	static const int		wait = settings["wait_for_connect"].toInt();
	static const QString	platform = settings["platform"].toCCharp();


	io.open(host, port, wait);
	io.write(
		IOTcpClient::MsgType::sign_up_data,platform.toLocal8Bit().data(),
		QString("login=" + user.getLogin() + ";password=" + user.getPassword() + ";" + 
				"username = "+user.getUsername()+";repassword="+user.getRePassword() +";")
	);
	io.waitForReadyRead(-1);
	vk::unsafe::Container_c body;
	vk::proto::VKCPv2::Header header;
	io.read(header, body);
	vk::unsafe::ObjectParser parser(body.toString().c_str());
	
	if (strcmp(parser["status"]->data(), "ok") == 0) {
		LogIn::setAutoLoginFile(user);
	} else {
		message = QString("Error: ") + parser["status"]->data();
	}
	io.close();
}

QString SignUp_Model::getMessage() const noexcept{
	return message;
}

SignUp_Model::SignUp_Model() noexcept(false){
	settings.setPath("config.probj").loadSettings();
}

SignUp_Model::~SignUp_Model(){

}
