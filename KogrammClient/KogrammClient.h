#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_KogrammClient.h"

class KogrammClient : public QMainWindow{
    Q_OBJECT
public:
    KogrammClient(QWidget *parent = Q_NULLPTR);
private:
    Ui::KogrammClientClass ui;
};
