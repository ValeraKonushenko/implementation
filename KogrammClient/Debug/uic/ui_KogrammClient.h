/********************************************************************************
** Form generated from reading UI file 'KogrammClient.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KOGRAMMCLIENT_H
#define UI_KOGRAMMCLIENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_KogrammClientClass
{
public:
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *KogrammClientClass)
    {
        if (KogrammClientClass->objectName().isEmpty())
            KogrammClientClass->setObjectName(QStringLiteral("KogrammClientClass"));
        KogrammClientClass->resize(600, 400);
        centralWidget = new QWidget(KogrammClientClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setEnabled(true);
        KogrammClientClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(KogrammClientClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        KogrammClientClass->setStatusBar(statusBar);

        retranslateUi(KogrammClientClass);

        QMetaObject::connectSlotsByName(KogrammClientClass);
    } // setupUi

    void retranslateUi(QMainWindow *KogrammClientClass)
    {
        KogrammClientClass->setWindowTitle(QApplication::translate("KogrammClientClass", "KogrammClient", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class KogrammClientClass: public Ui_KogrammClientClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KOGRAMMCLIENT_H
