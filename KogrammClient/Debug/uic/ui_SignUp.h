/********************************************************************************
** Form generated from reading UI file 'SignUp.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGNUP_H
#define UI_SIGNUP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SignUp
{
public:
    QPushButton *btnSignUp;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit_password;
    QLineEdit *lineEdit_login;
    QLineEdit *lineEdit_repassword;
    QLabel *label_3;
    QLineEdit *lineEdit_username;
    QLabel *label_4;
    QLabel *label_message;

    void setupUi(QWidget *SignUp)
    {
        if (SignUp->objectName().isEmpty())
            SignUp->setObjectName(QStringLiteral("SignUp"));
        SignUp->resize(415, 347);
        btnSignUp = new QPushButton(SignUp);
        btnSignUp->setObjectName(QStringLiteral("btnSignUp"));
        btnSignUp->setGeometry(QRect(140, 300, 101, 31));
        label = new QLabel(SignUp);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 160, 47, 13));
        label_2 = new QLabel(SignUp);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(50, 10, 47, 13));
        lineEdit_password = new QLineEdit(SignUp);
        lineEdit_password->setObjectName(QStringLiteral("lineEdit_password"));
        lineEdit_password->setGeometry(QRect(50, 180, 311, 31));
        lineEdit_login = new QLineEdit(SignUp);
        lineEdit_login->setObjectName(QStringLiteral("lineEdit_login"));
        lineEdit_login->setGeometry(QRect(50, 30, 311, 31));
        lineEdit_repassword = new QLineEdit(SignUp);
        lineEdit_repassword->setObjectName(QStringLiteral("lineEdit_repassword"));
        lineEdit_repassword->setGeometry(QRect(50, 240, 311, 31));
        label_3 = new QLabel(SignUp);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(50, 220, 161, 16));
        lineEdit_username = new QLineEdit(SignUp);
        lineEdit_username->setObjectName(QStringLiteral("lineEdit_username"));
        lineEdit_username->setGeometry(QRect(50, 100, 311, 31));
        label_4 = new QLabel(SignUp);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(50, 80, 61, 16));
        label_message = new QLabel(SignUp);
        label_message->setObjectName(QStringLiteral("label_message"));
        label_message->setGeometry(QRect(46, 280, 321, 20));
        QWidget::setTabOrder(lineEdit_login, lineEdit_username);
        QWidget::setTabOrder(lineEdit_username, lineEdit_password);
        QWidget::setTabOrder(lineEdit_password, lineEdit_repassword);
        QWidget::setTabOrder(lineEdit_repassword, btnSignUp);

        retranslateUi(SignUp);

        QMetaObject::connectSlotsByName(SignUp);
    } // setupUi

    void retranslateUi(QWidget *SignUp)
    {
        SignUp->setWindowTitle(QApplication::translate("SignUp", "SignUp", Q_NULLPTR));
        btnSignUp->setText(QApplication::translate("SignUp", "Sign up", Q_NULLPTR));
        label->setText(QApplication::translate("SignUp", "Password", Q_NULLPTR));
        label_2->setText(QApplication::translate("SignUp", "Login", Q_NULLPTR));
        lineEdit_repassword->setText(QString());
        label_3->setText(QApplication::translate("SignUp", "Repeate password", Q_NULLPTR));
        lineEdit_username->setText(QString());
        label_4->setText(QApplication::translate("SignUp", "Username", Q_NULLPTR));
        label_message->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class SignUp: public Ui_SignUp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGNUP_H
