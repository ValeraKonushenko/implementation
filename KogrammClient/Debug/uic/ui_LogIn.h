/********************************************************************************
** Form generated from reading UI file 'LogIn.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGIN_H
#define UI_LOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LogIn
{
public:
    QPushButton *pushButton_LogIn;
    QLineEdit *lineEdit_login;
    QLineEdit *lineEdit_password;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QPushButton *pushButton_sign_up_ref;
    QLabel *label_message;
    QCheckBox *checkBox_auto_login;

    void setupUi(QWidget *LogIn)
    {
        if (LogIn->objectName().isEmpty())
            LogIn->setObjectName(QStringLiteral("LogIn"));
        LogIn->resize(400, 339);
        pushButton_LogIn = new QPushButton(LogIn);
        pushButton_LogIn->setObjectName(QStringLiteral("pushButton_LogIn"));
        pushButton_LogIn->setGeometry(QRect(150, 250, 101, 31));
        lineEdit_login = new QLineEdit(LogIn);
        lineEdit_login->setObjectName(QStringLiteral("lineEdit_login"));
        lineEdit_login->setGeometry(QRect(50, 70, 311, 31));
        lineEdit_password = new QLineEdit(LogIn);
        lineEdit_password->setObjectName(QStringLiteral("lineEdit_password"));
        lineEdit_password->setGeometry(QRect(50, 150, 311, 31));
        label = new QLabel(LogIn);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 130, 47, 13));
        label_2 = new QLabel(LogIn);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(50, 50, 47, 13));
        label_3 = new QLabel(LogIn);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(130, 300, 81, 21));
        pushButton_sign_up_ref = new QPushButton(LogIn);
        pushButton_sign_up_ref->setObjectName(QStringLiteral("pushButton_sign_up_ref"));
        pushButton_sign_up_ref->setGeometry(QRect(210, 300, 75, 23));
        label_message = new QLabel(LogIn);
        label_message->setObjectName(QStringLiteral("label_message"));
        label_message->setGeometry(QRect(50, 220, 311, 21));
        checkBox_auto_login = new QCheckBox(LogIn);
        checkBox_auto_login->setObjectName(QStringLiteral("checkBox_auto_login"));
        checkBox_auto_login->setGeometry(QRect(50, 190, 371, 17));

        retranslateUi(LogIn);

        QMetaObject::connectSlotsByName(LogIn);
    } // setupUi

    void retranslateUi(QWidget *LogIn)
    {
        LogIn->setWindowTitle(QApplication::translate("LogIn", "LogIn", Q_NULLPTR));
        pushButton_LogIn->setText(QApplication::translate("LogIn", "Log In", Q_NULLPTR));
        label->setText(QApplication::translate("LogIn", "Password", Q_NULLPTR));
        label_2->setText(QApplication::translate("LogIn", "Login", Q_NULLPTR));
        label_3->setText(QApplication::translate("LogIn", "Not a member?", Q_NULLPTR));
        pushButton_sign_up_ref->setText(QApplication::translate("LogIn", "Sign up now!", Q_NULLPTR));
        label_message->setText(QString());
        checkBox_auto_login->setText(QApplication::translate("LogIn", "Remember me", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class LogIn: public Ui_LogIn {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGIN_H
