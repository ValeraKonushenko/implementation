#pragma once

#include "../vk/Logger.h"
#include "../vk/chat/VKCPv2.h"
#include <qobject.h>
#include <QtNetwork>
#include <string>
#include <functional>



class IOTcpClient : public QObject{
/*-----------------------------ENVIRONMENT-----------------------------------*/
public:
	using LOG = vk::safe::Logger;
	typedef vk::proto::VKCPv2::Header Header;
	typedef vk::proto::VKCPv2::MessageType MsgType;
	typedef vk::unsafe::Container<char> Container;



/*-----------------------------MAIN DATA-------------------------------------*/	
private:
	Q_OBJECT
	void			__init__		()									noexcept;
protected:
	QTcpSocket*		socket;

public slots:
	void			disconnected	();
	void			connected		();
	void			readyRead		();
signals:
	void			onRead			(QTcpSocket*, const Header&, const Container&);
	void			onConnect		(QTcpSocket*);
	void			onDisconnect	(QTcpSocket*);

public:
	void			write			(MsgType type, const char* platform, QString body)noexcept(false);
	void			write			(MsgType type, const char* platform, const char*data, size_t len)noexcept(false);
	void			read			(Header& header, Container& body)	noexcept(false);
	bool			waitForBytesWritten	(int msec)						const noexcept;
	bool			waitForConnected	(int msec)						const noexcept;
	bool			waitForDisconnected	(int msec)						const noexcept;
	bool			waitForReadyRead	(int msec)						const noexcept;
	bool			isOpen			()									const noexcept;
	void			open			(QString host, long long port, int wait)noexcept(false);
	void			close			()									noexcept;
	QString			peerHost		()									const noexcept;
	quint16			peerPort		()									const noexcept;
					IOTcpClient		()									noexcept;
					~IOTcpClient	();
					IOTcpClient		(const IOTcpClient& obj)			= delete;
					IOTcpClient		(IOTcpClient&& obj)					= delete;
	IOTcpClient&	operator=		(const IOTcpClient& obj)			= delete;
	IOTcpClient&	operator=		(IOTcpClient&& obj)					= delete;
};