#pragma once
#include "../vk/Logger.h"
#include "../vk/chat/VKCPv2.h"
#include <qobject.h>
#include <QtNetwork>
#include <string>
#include <functional>



class IOTcpServer : public QObject{
/*-----------------------------ENVIRONMENT-----------------------------------*/
public:
	using Proto = vk::proto::VKCPv2;
	using LOG = vk::safe::Logger;
	typedef vk::proto::VKCPv2::Header Header;
	typedef vk::unsafe::Container<char> Container;
	typedef vk::proto::VKCPv2::MessageType MsgType;



/*-----------------------------MAIN DATA-------------------------------------*/	
private:
	Q_OBJECT
	void			__init__		()									noexcept;
protected:
	QTcpServer*		server;

public slots:
	void			disconnected	();
	void			connected		();
	void			readyRead		();
signals:
	void			onRead			(QTcpSocket*, const Header&, const Container&);
	void			onConnect		(QTcpSocket*);
	void			onDisconnect	(QTcpSocket*);

public:
	void			write			(QTcpSocket* dst, QString body)						noexcept(false);
	void			write			(QTcpSocket* dst, const char* data, size_t len)		noexcept(false);
	bool			isOpen			()									const noexcept;
	void			open			(QString host, long long port)		noexcept(false);
	void			close			()									noexcept;
	QString			peerHost		()									const noexcept;
	quint16			peerPort		()									const noexcept;
					IOTcpServer		()									noexcept;
					~IOTcpServer	();
					IOTcpServer		(const IOTcpServer& obj)			= delete;
					IOTcpServer		(IOTcpServer&& obj)					noexcept;
	IOTcpServer&	operator=		(const IOTcpServer& obj)			= delete;
	IOTcpServer&	operator=		(IOTcpServer&& obj)					noexcept;
};