#include "DataBaseUsers.h"
#include "../vk/Assert.h"
#include "../vk/parser/ObjectParser.h"
#include "../vk/chat/GUIException.h"

DataBaseUsers::DataBaseUsers() noexcept(false) : 
	db(DataBase::instance()),
	user_table(db.getSettings()["users_table_name"].c_str()){

}



QString DataBaseUsers::getUsername(const User& user) const noexcept(false){
	QString query = "SELECT username FROM " + user_table +
		" WHERE login = '" + user.getLogin() + "' "
		"AND password = '" + user.getPassword() + "'";
	auto q = db.query(query);
	q.next();
	QString username = q.value(0).toString();
	return username;	
}

void DataBaseUsers::push(const User& user) noexcept(false) {
	using namespace std;
	try {
		User::isValidUser(user);

		isUser(user);

		QString query = 
			"INSERT INTO " + user_table + "(login, password, username)"
			" VALUES('"+user.getLogin()+"','" + user.getPassword() + "','" + 
					user.getUsername() + "')";

		db.query(query);
	} catch (const std::runtime_error&ex) {
		throw vk::GUIException(ex.what());
	}
}
void DataBaseUsers::findAndRemove(const QString& username) noexcept(false){
	
}

void DataBaseUsers::isUser(const User& user) const noexcept(false){
	QString query = "SELECT 1 FROM " + user_table +
		" WHERE login = '" + user.getLogin() + "'";
	auto q = db.query(query);
	int amo = 0;
	while (q.next())amo++;
	if (amo != 0)
		throw std::runtime_error("The user with such login already exist");

	query = "SELECT 1 FROM " + user_table +
		" WHERE username = '" + user.getUsername() + "'";
	q = db.query(query);
	amo = 0;
	while (q.next())amo++;
	if (amo != 0)
		throw std::runtime_error("The user with such username already exist");
}

bool DataBaseUsers::isUserForLogin(const User& user) const noexcept(false){
	QString query = "SELECT 1 FROM " + user_table +
		" WHERE login = '" + user.getLogin() + "' AND password = '" +user.getPassword()+ "';";
	auto q = db.query(query);
	int amo = 0;
	while (q.next())amo++;
	if (amo == 0)
		return false;
	return true;
}

User DataBaseUsers::getUser(const QString& username) const noexcept(false){
	return User();
}
