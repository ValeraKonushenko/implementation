#include "KogrammServer.h"
#include "../vk/chat/VKCPv2.h"
#include "../vk/Assert.h"
#include "../vk/chat/GUIException.h"
#include <qnetworkinterface.h>
#include <QtNetwork>
#include <qmessagebox.h>



KogrammServer::KogrammServer(QWidget *parent)noexcept(false)
    : QMainWindow(parent){
    settings.setPath("config.probj").loadSettings();
    
    //UI INIT
    ui.setupUi(this);

    //LOGGER
    log_sb.set(LOG::Events::push_log, [this]() {
        ui.listWidget_Logs->addItem(LOG::getLastLog().message().c_str());
    });
    LOG::subscribe(log_sb);

    //SERVER socket
    server.open(settings["host"].toCCharp(), settings["port"].toInt());
    LOG::push("Listening...");
    connect(&server, &IOTcpServer::onConnect, this, &KogrammServer::onConnect);
    connect(&server, &IOTcpServer::onDisconnect, this, &KogrammServer::onDisconnect);
    connect(&server, &IOTcpServer::onRead, this, &KogrammServer::onRead);
}
KogrammServer::~KogrammServer(){
}




void KogrammServer::processMesasgeReq(
    QTcpSocket* client, 
    const IOTcpServer::Header& header, 
    const IOTcpServer::Container& body) noexcept(false)
{
    if (header.type != vk::proto::VKCPv2::MessageType::message)
        throw vk::exception("The Header's type not compatible with the function of processing", __FILE__, __FUNCTION__, __LINE__);
    //	//throw vk::exception("Too big length of the useragent", __FILE__, __FUNCTION__, __LINE__);

}
void KogrammServer::processLogInReq(
    QTcpSocket* client,
    const IOTcpServer::Header& header,
    const IOTcpServer::Container& body) noexcept(false)
{
    if (header.type != vk::proto::VKCPv2::MessageType::log_in_data)
        throw vk::exception("The Header's type not compatible with the function of processing", __FILE__, __FUNCTION__, __LINE__);
    try {
        vk::unsafe::ObjectParser parser(body.toString().c_str());
        User user;
        user.setLogin(parser["login"]->data());
        user.setPassword(parser["password"]->data());
        if (!user_db.isUserForLogin(user)) {
            server.write(client, "status = No such user;");            
            return;
        }
        server.write(client, "status = ok;username = "+user_db.getUsername(user)+";");
        LOG::push(("The user: "+user.getLogin()+" was logged in").toLocal8Bit().data());
    } catch (const std::runtime_error&ex) {
        server.write(client, "status = error;error= "+QString(ex.what())+";");
    }
}
void KogrammServer::processSignUpReq(QTcpSocket* client, 
const IOTcpServer::Header& header, const IOTcpServer::Container& body) noexcept(false){
    if (header.type != vk::proto::VKCPv2::MessageType::sign_up_data)
        throw vk::exception("The Header's type not compatible with the function of processing", __FILE__, __FUNCTION__, __LINE__);
    vk::unsafe::ObjectParser parser(body.toString().c_str());
    User user;
    user.setLogin(parser["login"]->data());
    user.setPassword(parser["password"]->data());
    user.setRePassword(parser["repassword"]->data());
    user.setUsername(parser["username"]->data());
    try {
        user_db.push(user);
    } catch (const vk::GUIException&ex) {
        server.write(client, "status = "+QString(ex.what())+";");
        return;
    }
    server.write(client, "status = ok;");
}
void KogrammServer::processOtherReq(QTcpSocket* client, 
const IOTcpServer::Header& header, const IOTcpServer::Container& body) noexcept(false){
    if (header.type != vk::proto::VKCPv2::MessageType::other)
        throw vk::exception("The Header's type not compatible with the function of processing", __FILE__, __FUNCTION__, __LINE__);

}




void KogrammServer::closeEvent(QCloseEvent* event){

}
void KogrammServer::__init_server__() noexcept{

    statusBar()->showMessage(tr("The server is running on\n\nIP: %1\nport: %2\n\nRun the Fortune Client example now.")
           .arg(server.peerHost()).arg(server.peerPort()));

}

void KogrammServer::onRead(QTcpSocket* client, const IOTcpServer::Header& header,
                           const IOTcpServer::Container& body){
    LOG::push(("Data were sent from: " + client->peerAddress().toString() + ":" + QString::number(client->peerPort())).toLocal8Bit().data());
    if (header.version != 2.0) {
        LOG::push("Incompatible VKCP's version");
        return;           
    }
    using MT = vk::proto::VKCPv2::MessageType;

    switch (header.type) {
        case MT::none:
            LOG::push("Unknow message format from the client");
            break;
        case MT::message:
            processMesasgeReq(client, header, body);
            break;
        case MT::log_in_data:
            processLogInReq(client, header, body);
            break;
        case MT::sign_up_data:
            processSignUpReq(client, header, body);
            break;
        case MT::other:
            processOtherReq(client, header, body);
            break;
        default:
            LOG::push("Unknow message format from the client");
            break;
    }

}

void KogrammServer::onConnect(QTcpSocket* client){
    LOG::push(("Connected new client: " + client->peerAddress().toString() + ":" + QString::number(client->peerPort())).toLocal8Bit().data());

}

void KogrammServer::onDisconnect(QTcpSocket* client){
    if (client)
        LOG::push("Some client was disconnected");
    else
        LOG::push(("Was disconnnectd: " + client->peerAddress().toString() + ":" + QString::number(client->peerPort())).toLocal8Bit().data());

}
