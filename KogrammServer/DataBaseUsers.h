#pragma once
#include "DataBase.h"
#include "../general/User.h"
#include <qstring>
class DataBaseUsers {
protected:
	DataBase& db;
	const QString user_table;
public:
	QString			getUsername		(const User& user)				const noexcept(false);
	void			push			(const User& user)				noexcept(false);
	void			findAndRemove	(const QString& username)	noexcept(false);
	void			isUser			(const User& user)				const noexcept(false);
	bool			isUserForLogin(const User& user)				const noexcept(false);
	User			getUser			(const QString& username)	const noexcept(false);
					DataBaseUsers	()								noexcept(false);
					~DataBaseUsers	()								= default;
					DataBaseUsers	(const DataBaseUsers& obj)		= delete;
					DataBaseUsers	(DataBaseUsers&& obj)			= delete;
	DataBaseUsers&	operator=		(const DataBaseUsers& obj)		= delete;
	DataBaseUsers&	operator=		(DataBaseUsers&& obj)			= delete;
};