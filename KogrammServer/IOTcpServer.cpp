#include "IOTcpServer.h"
#include <QTcpServer>
void IOTcpServer::__init__() noexcept{
	server = nullptr;
}

void IOTcpServer::disconnected(){
	emit onDisconnect(qobject_cast<QTcpSocket*>(sender()));
}
void IOTcpServer::connected(){
	QTcpSocket* client = server->nextPendingConnection();
	connect(client, &QAbstractSocket::disconnected, this, &IOTcpServer::disconnected);
	connect(client, &QAbstractSocket::readyRead, this, &IOTcpServer::readyRead);
	emit onConnect(client);
}
void IOTcpServer::readyRead(){
	QTcpSocket* client = qobject_cast<QTcpSocket*>(sender());
	if (!client) {
		LOG::push("Was recieved an empty client");
		return;
	}


	Container header_raw(vk::proto::VKCPv2::size_of_header);
	client->read(header_raw, header_raw.getSize());
	Header header = vk::proto::VKCPv2::parse(header_raw);
	Container body_raw(header.body_size);
	client->read(body_raw, header.body_size);
	emit onRead(client, header, body_raw);
}


void IOTcpServer::write(QTcpSocket* dst, QString body) noexcept(false){
	this->write(dst, body.toLocal8Bit().data(), body.length());
}
void IOTcpServer::write(QTcpSocket* dst, const char* data, size_t len) noexcept(false){

	Proto::Status status;
	status.type = Proto::StatusType::ok;
	auto header = Proto::generate(
		Proto::proto_version,
		Proto::MessageType::answer,
		len,
		"server",
		status
	);	
	dst->write(header, header.getSize());
	dst->write(data, len);
}


bool IOTcpServer::isOpen() const noexcept{
	return server != nullptr;
}
void IOTcpServer::open(QString host, long long port) noexcept(false) {
	if (isOpen())close();

	server = new QTcpServer();
	if (!server)
		throw  std::exception("Error of allocating the memory for the server's socet");

	if (!server->listen(QHostAddress(host), static_cast<quint16>(port)))
		throw  std::exception("Error with listening");

	connect(server, &QTcpServer::newConnection, this, &IOTcpServer::connected);
}
void IOTcpServer::close() noexcept{
	if (!server)return;
	server->close();
	delete server;
}
QString IOTcpServer::peerHost() const noexcept{
	return server->serverAddress().toString();
}
quint16 IOTcpServer::peerPort() const noexcept{
	return quint16(-1);
}
IOTcpServer::IOTcpServer() noexcept{
	__init__();
}
IOTcpServer::~IOTcpServer(){
	close();
}
IOTcpServer::IOTcpServer(IOTcpServer&& obj) noexcept{
	*this = std::move(obj);
}
IOTcpServer& IOTcpServer::operator=(IOTcpServer&& obj) noexcept{
	server = std::move(obj.server);
	obj.__init__();
	return *this;
}
