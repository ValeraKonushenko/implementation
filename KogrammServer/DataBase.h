#pragma once
#include "../vk/codeArchitecture/Settings.h"
#include <mutex>
#include <qsqldatabase.h>
#include <functional>
#include <qvariant.h>
#include <qsqlerror.h>
#include <qsqlquery.h>

class DataBase {
private:
	DataBase()noexcept(false);
protected:
	mutable std::recursive_mutex rec_mut;
	QSqlDatabase		db;
	QString				path;
	QString				driver;
	vk::safe::Settings	settings;

public:
	const vk::safe::Settings& getSettings()const;
	QSqlQuery			query		(QString query)		noexcept(false);
	static DataBase&	instance	()					noexcept(false);

	~DataBase()											= default;
	DataBase(const DataBase&)							= delete;
	DataBase(DataBase&&)								= delete;
	DataBase& operator=(const DataBase&)				= delete;
	DataBase& operator=(DataBase&&)						= delete;
};