#include "KogrammServer.h"
#include <QtWidgets/QApplication>
#include "DataBaseUsers.h"
#include "../vk/chat/GUIException.h"
#include <windows.h>

int main(int argc, char* argv[]) {
	try {        
        QApplication a(argc, argv);
        KogrammServer w;
        w.show();
        return a.exec();
	} catch (const std::runtime_error&ex) {
        MessageBoxA(nullptr, ex.what(), "Critical error!", MB_ICONERROR);
    } catch (const vk::GUIException&ex) {
        MessageBoxA(nullptr, ex.what(), "GUI error", MB_ICONERROR);
    } catch (...) {
        MessageBoxA(nullptr, "Has occured some error", "Critical error!", MB_ICONERROR);
    }
}
