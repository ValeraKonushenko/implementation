#include "DataBase.h"
#include "../vk/Destroyer.h"
#include "../vk/Logger.h"
#include "../vk/Assert.h"
#include <qdir.h>
#include <exception>

DataBase::DataBase()noexcept(false) {
	settings.setPath("config.probj").loadSettings();
	driver = settings["db_driver"].c_str();
	if (!QSqlDatabase::isDriverAvailable(driver))
		throw std::exception(("Driver: " + driver + " - not aviable").toLocal8Bit().data());
	db = QSqlDatabase::addDatabase(driver);
	path = settings["path_to_database"].c_str();
	QString name = "Driver={Microsoft Access Driver (*.mdb)};DSN='';DBQ="
			+ QDir::toNativeSeparators(QDir::currentPath() + "/" + path);

	db.setDatabaseName(name);
	if (!db.open())
		throw vk::exception((std::string(
			"Can't open the connection to database. May be incorrect "
			"path, or no such file. Datail: ") + std::string(db.lastError()
									.text().toLocal8Bit().data())), __FILE__, __FUNCTION__, __LINE__);
	
}

const vk::safe::Settings& DataBase::getSettings() const{
	return settings;
}

QSqlQuery DataBase::query(QString query) noexcept(false){
	if (query == "")
		throw vk::exception("Gotten query was an empty", __FILE__, __FUNCTION__, __LINE__);
	QSqlQuery q;
	if(!q.exec(query))
		throw vk::exception(("Query execution was unsuccessfully: " + q.lastError().text()).toLocal8Bit().data(), __FILE__, __FUNCTION__, __LINE__);

	return std::move(q);
}

DataBase& DataBase::instance() noexcept(false){
	static std::recursive_mutex		rec_mut;
	static vk::Destroyer<DataBase>	destroyer;
	static DataBase*				p = nullptr;
	if (!p) {
		std::lock_guard lg(rec_mut);
		if (!p) {
			p = new DataBase();
			if (!p)throw std::exception("Bad alloc");
			destroyer.set(p);
		}
	}
	return *p;
}
