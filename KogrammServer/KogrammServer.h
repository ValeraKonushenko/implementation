#pragma once

#include "ui_KogrammServer.h"
#include "../vk/Logger.h"
#include "../vk/observer/Subscriber.h"
#include "../vk/codeArchitecture/Settings.h"
#include "DataBaseUsers.h"
#include "IOTcpServer.h"
#include <vector>
#include <QtWidgets/QMainWindow>
#include <qtcpserver.h>

class KogrammServer : public QMainWindow{
/*-----------------------------ENVIRONMENT-----------------------------------*/
public:
    using LOG = vk::safe::Logger;

/*-----------------------------MAIN DATA-------------------------------------*/
protected:

    Q_OBJECT
    vk::safe::Settings      settings;
    Ui::KogrammServerClass  ui;
    vk::safe::Subscriber    log_sb;
    IOTcpServer             server;
    DataBaseUsers           user_db;
    virtual void processMesasgeReq(QTcpSocket* client, const IOTcpServer::Header& header,
                                const IOTcpServer::Container& body)noexcept(false);
    virtual void processLogInReq(QTcpSocket* client, const IOTcpServer::Header& header,
                                const IOTcpServer::Container& body)noexcept(false);
    virtual void processSignUpReq(QTcpSocket* client, const IOTcpServer::Header& header,
                                const IOTcpServer::Container& body)noexcept(false);
    virtual void processOtherReq(QTcpSocket* client, const IOTcpServer::Header& header,
                                const IOTcpServer::Container& body)noexcept(false);

    virtual void closeEvent(QCloseEvent* event)     override;

    void    __init_server__         ()              noexcept;
public slots:    
    void	onRead			(QTcpSocket* client, const IOTcpServer::Header& header, const IOTcpServer::Container& body);
	void	onConnect		(QTcpSocket* client);
	void	onDisconnect	(QTcpSocket* client);
public:
    KogrammServer(QWidget *parent = Q_NULLPTR)noexcept(false);
    ~KogrammServer();
};
