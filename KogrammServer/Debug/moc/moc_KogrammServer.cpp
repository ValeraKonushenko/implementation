/****************************************************************************
** Meta object code from reading C++ file 'KogrammServer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../KogrammServer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'KogrammServer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_KogrammServer_t {
    QByteArrayData data[11];
    char stringdata0[119];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KogrammServer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KogrammServer_t qt_meta_stringdata_KogrammServer = {
    {
QT_MOC_LITERAL(0, 0, 13), // "KogrammServer"
QT_MOC_LITERAL(1, 14, 6), // "onRead"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 11), // "QTcpSocket*"
QT_MOC_LITERAL(4, 34, 6), // "client"
QT_MOC_LITERAL(5, 41, 19), // "IOTcpServer::Header"
QT_MOC_LITERAL(6, 61, 6), // "header"
QT_MOC_LITERAL(7, 68, 22), // "IOTcpServer::Container"
QT_MOC_LITERAL(8, 91, 4), // "body"
QT_MOC_LITERAL(9, 96, 9), // "onConnect"
QT_MOC_LITERAL(10, 106, 12) // "onDisconnect"

    },
    "KogrammServer\0onRead\0\0QTcpSocket*\0"
    "client\0IOTcpServer::Header\0header\0"
    "IOTcpServer::Container\0body\0onConnect\0"
    "onDisconnect"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KogrammServer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    3,   29,    2, 0x0a /* Public */,
       9,    1,   36,    2, 0x0a /* Public */,
      10,    1,   39,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 5, 0x80000000 | 7,    4,    6,    8,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,

       0        // eod
};

void KogrammServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        KogrammServer *_t = static_cast<KogrammServer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onRead((*reinterpret_cast< QTcpSocket*(*)>(_a[1])),(*reinterpret_cast< const IOTcpServer::Header(*)>(_a[2])),(*reinterpret_cast< const IOTcpServer::Container(*)>(_a[3]))); break;
        case 1: _t->onConnect((*reinterpret_cast< QTcpSocket*(*)>(_a[1]))); break;
        case 2: _t->onDisconnect((*reinterpret_cast< QTcpSocket*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QTcpSocket* >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QTcpSocket* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QTcpSocket* >(); break;
            }
            break;
        }
    }
}

const QMetaObject KogrammServer::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_KogrammServer.data,
      qt_meta_data_KogrammServer,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *KogrammServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KogrammServer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KogrammServer.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int KogrammServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
