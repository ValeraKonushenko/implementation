/********************************************************************************
** Form generated from reading UI file 'KogrammServer.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KOGRAMMSERVER_H
#define UI_KOGRAMMSERVER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_KogrammServerClass
{
public:
    QWidget *centralWidget;
    QListWidget *listWidget_Logs;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *KogrammServerClass)
    {
        if (KogrammServerClass->objectName().isEmpty())
            KogrammServerClass->setObjectName(QStringLiteral("KogrammServerClass"));
        KogrammServerClass->resize(600, 400);
        centralWidget = new QWidget(KogrammServerClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        listWidget_Logs = new QListWidget(centralWidget);
        listWidget_Logs->setObjectName(QStringLiteral("listWidget_Logs"));
        listWidget_Logs->setGeometry(QRect(15, 11, 431, 371));
        KogrammServerClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(KogrammServerClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        KogrammServerClass->setStatusBar(statusBar);

        retranslateUi(KogrammServerClass);

        QMetaObject::connectSlotsByName(KogrammServerClass);
    } // setupUi

    void retranslateUi(QMainWindow *KogrammServerClass)
    {
        KogrammServerClass->setWindowTitle(QApplication::translate("KogrammServerClass", "KogrammServer", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class KogrammServerClass: public Ui_KogrammServerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KOGRAMMSERVER_H
